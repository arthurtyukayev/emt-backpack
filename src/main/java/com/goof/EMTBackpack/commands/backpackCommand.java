package com.goof.EMTBackpack.commands;

import com.goof.EMTBackpack.EMTBackpack;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Arthur on 4/13/2015.
 */
public class backpackCommand implements CommandExecutor {

    EMTBackpack plugin;

    public backpackCommand(EMTBackpack passedPlugin) {
        this.plugin = passedPlugin;
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player player = (Player) sender;

        if (sender instanceof Player) {
            if (label.equalsIgnoreCase("backpack")){
                if (player.hasPermission("emtbackpack.command.openbackpack")) {
                    if (args.length == 0) {
                        player.openInventory(plugin.playerInventories.get(player.getUniqueId()));
                        return true;
                    } else if (args.length == 1) {
                        if (player.isOp()) {
                            UUID targetUUID = null;
                            for (HashMap.Entry<String, UUID> set : plugin.emtsql.nameUUIDs.entrySet()) {
                                if (set.getKey().equalsIgnoreCase(args[0])) {
                                    targetUUID = set.getValue();
                                }
                            }
                            player.openInventory(plugin.playerInventories.get(targetUUID));
                        } else {
                            player.sendMessage(ChatColor.RED + "You're not allowed to use this command in this way.");
                            player.sendMessage(ChatColor.RED + "You're not allowed to specify player names in command arguments.");
                        }
                    }
                } else {
                    sender.sendMessage(ChatColor.RED + "You do not have permission to use this command.");
                }
            }
        } else {
            sender.sendMessage(ChatColor.RED + "ERROR: This command is for players only.");
        }
        return true;
    }
}
