package com.goof.EMTBackpack.objects;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

/**
 * Created by Arthur on 4/9/2015.
 */
public class PlayerInventory {

    Player player;
    Inventory inventory;

    public PlayerInventory(Inventory passedInventory, Player passedPlayer) {
        this.inventory = passedInventory;
        this.player = passedPlayer;
    }

    public Player getPlayer() {
        return player;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }


}
