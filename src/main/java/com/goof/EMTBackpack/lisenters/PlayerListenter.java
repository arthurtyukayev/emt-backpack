package com.goof.EMTBackpack.lisenters;

import com.goof.EMTBackpack.EMTBackpack;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class PlayerListenter implements Listener {

    EMTBackpack plugin;

    public PlayerListenter(EMTBackpack passedPlugin) {
        this.plugin = passedPlugin;
    }

    @EventHandler
    public void playerJoinEvent(PlayerJoinEvent event) {
        Player joiner = event.getPlayer();
        if (plugin.playerInventories.get(joiner.getUniqueId()) == null) {
            Inventory inventory = Bukkit.createInventory(joiner, 27, joiner.getDisplayName() + "'s Backpack");
            plugin.playerInventories.put(joiner.getUniqueId(), inventory);
        }
    }

//    @EventHandler
//    public void playerQuitEvent(PlayerQuitEvent event) {
//        Player quitter = event.getPlayer();
//        Inventory quitterBackpack = plugin.playerInventories.get(quitter.getUniqueId());
//
//        plugin.emtsql.playerStats.get(quitter.getUniqueId()).setEncodedBackpack(plugin.itemSerialization.toBase64(quitterBackpack));
//    }

    @EventHandler
    public void playerDeathEvent(PlayerDeathEvent event) {
        Player player = event.getEntity().getPlayer();
        Inventory inventory = plugin.playerInventories.get(player.getUniqueId());
        Location deathLocation = event.getEntity().getLocation();

        for (ItemStack item : plugin.playerInventories.get(player.getUniqueId()).getContents()){
            deathLocation.getWorld().dropItem(deathLocation, item);
            inventory.remove(item);
        }
    }

    @EventHandler
    public void invetoryInteractEvent(InventoryCloseEvent event) {
        Player player = (Player) event.getPlayer();
        if (event.getInventory().equals(plugin.playerInventories.get(player.getUniqueId()))) {
            plugin.emtsql.playerStats.get(player.getUniqueId()).setEncodedBackpack(plugin.itemSerialization.toBase64(plugin.playerInventories.get(player.getUniqueId())));
        }
    }
}
