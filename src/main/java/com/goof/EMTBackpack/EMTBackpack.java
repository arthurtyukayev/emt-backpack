package com.goof.EMTBackpack;

import com.goof.EMTBackpack.commands.backpackCommand;
import com.goof.EMTBackpack.lisenters.PlayerListenter;
import com.goof.EMTBackpack.utils.ItemSerialization;
import com.goof.EMTBackpack.utils.MultipleConfigs;
import com.goof.EMTSQL.EMTSQL;
import com.goof.EMTSQL.objects.PlayerStats;
import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.UUID;
import java.util.logging.Level;

public class EMTBackpack extends JavaPlugin{

    public ItemSerialization itemSerialization = new ItemSerialization();
    public HashMap<UUID, Inventory> playerInventories = new HashMap<UUID, Inventory>();
    public MultipleConfigs config = new MultipleConfigs(this);
    public EMTSQL emtsql = (EMTSQL) Bukkit.getPluginManager().getPlugin("EMTSQL");


    public void onEnable(){
        getServer().getPluginManager().registerEvents(new PlayerListenter(this), this);
        getCommand("backpack").setExecutor(new backpackCommand(this));

        getLogger().log(Level.INFO, "Getting all player backpack inventories.");
        for (HashMap.Entry<UUID, PlayerStats> set : emtsql.playerStats.entrySet()){
            playerInventories.put(set.getKey(), itemSerialization.fromBase64(set.getValue().getEncodedBackpack()));
        }
        getLogger().log(Level.INFO, "Retrieved " + emtsql.playerStats.size() + " inventories.");
        getLogger().log(Level.INFO, "playerInventories HashMap currently holding " + playerInventories.size() + " entries ((UUID, Inventory) sets).");
    }
    public void onDisable(){
        getLogger().log(Level.INFO, "Serializing all player inventories...");
        for (HashMap.Entry<UUID, Inventory> set : playerInventories.entrySet()){
            emtsql.playerStats.get(set.getKey()).setEncodedBackpack(itemSerialization.toBase64(set.getValue()));
        }
        getLogger().log(Level.INFO, "Serialization complete.");
    }
}
